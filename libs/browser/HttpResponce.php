<?php
	class HttpResponce
	{
		private $_headers = [];
		private $_body = '';

		function __construct( $responce )
		{
			$this->parse($responce);
		}

		public function headers()
		{
			return $this->_headers;
		}

		public function body()
		{
			return $this->_body;
		}

		private function parse($resp)
		{
			$isHeaders = true;
			foreach(explode("\n", $resp) as $line)
			{
				if($isHeaders)
				{
					if(empty($line) || $line === "\r")
					{
						$isHeaders = false;
						continue;
					}

					if(strpos($line, 'HTTP/') !== false)
					{
						if(preg_match('#^HTTP/([\.\d]+) ([\d]+) (.*?)$#', $line, $match))
						{
							$this->version = $match[1];
							$this->code = $match[2];
							$this->codeText = $match[3];
						}
					}
					else
					{
						$header = array_map('trim', explode(':', $line));
						if(count($header) == 2)
							$this->_headers[$header[0]] = $header[1];
					}
				}
				else
					$this->_body .= $line."\n";
			}
		}
	}